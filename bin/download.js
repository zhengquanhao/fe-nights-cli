const fs = require("fs-extra");
const path = require('path');
const ora = require("ora");
const chalk = require('chalk');
const downloadGitRepo = require('download-git-repo');
const handlebars = require('handlebars');
const inquirer = require("inquirer");
const spawn = async (...args) => {
    const { spawn } = require('child_process')
    return new Promise(resolve => {
        const proc = spawn(...args)
        proc.stdout.pipe(process.stdout)
        proc.stderr.pipe(process.stderr)
        proc.on('close', () => {
            resolve()
        })
    })
}

module.exports = function (name, res) {
    let params = { name, ...res }
    const spinner = ora('正在下载模板, 请稍后...');
    spinner.start();
    downloadGitRepo('direct:https://代码地址.git', name, { clone: true }, (error) => {
        if (!error) {
            spinner.succeed();
            const packagePath = path.join(name, 'package.json');
            console.log("packagePath", packagePath);
            // 判断是否有package.json, 要把输入的数据回填到模板中
            if (fs.existsSync(packagePath)) {
                const content = fs.readFileSync(packagePath).toString();
                // handlebars 模板处理引擎
                const template = handlebars.compile(content);
                const result = template(params);
                fs.writeFileSync(packagePath, result);
                console.log(chalk.green('success! 项目初始化成功！'));

                async function install(name) {
                    const { next } = await inquirer.prompt([
                        {
                            type: 'confirm',
                            name: 'next',
                            message: '继续安装依赖',
                            default: true
                        }
                    ])
                    if (next) {
                        const spinner = ora('正在安装依赖中......');
                        await spinner.start();
                        await spawn("npm", ["install"], { cwd: `./${name}` })
                        await spinner.succeed();
                        await console.log(
                            chalk.greenBright('安装完成，开启项目') + '\n' +
                            chalk.greenBright('cd ' + name) + '\n' +
                            chalk.greenBright('开始开发吧!')
                        )
                    }
                }
                install(name);

            } else {
                console.log('failed! no package.json');
            }
        }
    })
}