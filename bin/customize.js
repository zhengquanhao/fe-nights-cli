// const path = require("path");
const inquirer = require("inquirer");
const download = require("./download");

module.exports = function (name) {
    inquirer.prompt([
        {
            name: 'author',
            message: 'Please enter the project author',
            default: 'fe-nights'
        },
        {
            name: 'description',
            message: 'Please enter a project description'
        },
        {
            name: "template",
            type: "list",
            message: "Please choose a template to create project",
            choices: [ "vue-template", "react-template" ],
            default: "vue-template"
        }
    ]).then(res => {
        download(name, res);
    })
}