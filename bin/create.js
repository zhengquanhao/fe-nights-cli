const path = require("path");
const fs = require("fs-extra");
const ora = require("ora");
const inquirer = require("inquirer");
const customize = require("./customize");

module.exports = async function (name, options) {
    // 执行创建命令
    // 当前命令行选择的目录
    const cwd  = process.cwd();
    // 需要创建的目录地址
    const targetAir  = path.join(cwd, name);
    // 覆盖旧的目录
    const removeDir = () => {
        const spinner = ora(`\r\nRemoving...`);
        spinner.start();
        fs.remove(targetAir);
        spinner.succeed();
    }
    // 目录是否已经存在？
    if (fs.existsSync(targetAir)) {
        // 是否为强制创建？
        if (options.force) {
            removeDir();
            customize(name);
        } else {
            // 询问用户是否确定要覆盖
            let { action } = await inquirer.prompt([
                {
                    name: 'action',
                    type: 'list',
                    message: 'Target directory already exists Pick an action:',
                    choices: [
                        {
                            name: 'Overwrite',
                            value: true
                        },{
                            name: 'Cancel',
                            value: false
                        }
                    ]
                }
            ])
            if (action) {
                removeDir();
                customize(name);
            } else {
                console.log("\r\nThe same file exists, creation is terminated");
            }
        }
    } else {
        customize(name);
    }
}